<%-- 
    Document   : home
    Created on : Jan 8, 2020, 9:55:28 AM
    Author     : xuanvinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${sessionScope.loginUsername == null}">
    <jsp:forward page="login.jsp"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <link href="css/style.css" type="text/css" rel="stylesheet"/>
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    </head>
    <body>
        <div class="container">
            <h1>Welcome, ${sessionScope.loginUsername}</h1>
            
            <form action="PostServlet" method="POST">
                <input type="hidden" name="username" value="${sessionScope.loginUsername}"/>
                <input type="hidden" name="type" value="all"/>
                <input type="hidden" name="action" value="VIEW"/>
                <input style="position: absolute;" class="btn-post" type="submit" value="All Post"/>
            </form>
        
            <form action="PostServlet" method="POST">
                <input type="hidden" name="username" value="${sessionScope.loginUsername}"/>
                <input type="hidden" name="type" value="user"/>
                <input type="hidden" name="action" value="VIEW"/>
                <input class="post-btn-cmt btn-post" type="submit" value="My Post"/>
            </form>
            
            <form action="PostServlet" method="POST">
                <input type="hidden" name="username" value="${sessionScope.loginUsername}"/>
                <input class="box-post" type="text" name="content" placeholder="${sessionScope.loginUsername}, what are you thinking?"/>
                <input type="hidden" name="action" value="ADD"/><br>
                <input class="post-btn" type="submit" value="POST"/>
            </form>
        
            <div class="content-cmt">
                <c:forEach var="post" items="${getAllPost}">
                    <ul style="list-style-type:none;">
                        <li class="big-tittle">${post.username}</li>
                        <li class="sm-title">${post.createAt}</li>
                        <li>${post.content}</li>
                        <li>
                            <form action="CommentServlet" method="POST">
                                <input type="hidden" name="postId" value="${post.id}"/>
                                <input type="hidden" name="postUsername" value="${post.username}"/>
                                <input type="hidden" name="postContent" value="${post.content}"/>
                                <input type="hidden" name="postCreateAt" value="${post.createAt}"/>
                                <input type="hidden" name="action" value="VIEW"/>
                                <input class="btn-blue" type="submit" value="View Comment"/>
                            </form>
                        </li>
                    </ul>
                </c:forEach>
            </div>
        </div>
    </body>
</html>
