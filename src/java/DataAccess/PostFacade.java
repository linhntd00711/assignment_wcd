/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import Entity.Post;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author xuanvinh
 */
@Stateless
public class PostFacade extends AbstractFacade<Post> implements PostFacadeLocal {

    @PersistenceContext(unitName = "AssignmentPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PostFacade() {
        super(Post.class);
    }

    @Override
    public List<Post> listPost(String username, String type) {
        Query query = null;
        switch (type) {
            case "all": {
                query = em.createNativeQuery("select * from post order by create_at desc", Post.class);
                break;
            }
            case "user": {
                query = em.createNativeQuery("select * from post where username = ? order by create_at desc", Post.class);
                
                query.setParameter(1, username);
                break;
            }
        }
        
        List<Post> list = (List<Post>) query.getResultList();
        
        return list;
    }

    @Override
    public Post addPost(String username, String content) {
        Query query = em.createNativeQuery("insert into post(username, content, create_at) values(?, ?, ?)");
        
        Date date = new Date();
        
        query.setParameter(1, username);
        query.setParameter(2, content);
        query.setParameter(3, date);
        
        query.executeUpdate();
        
        query = em.createNativeQuery("select * from post where username = ? order by id desc fetch first row only", Post.class);
        query.setParameter(1, username);
        try {
            Post row = (Post) query.getSingleResult();
            
            return row;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
}
