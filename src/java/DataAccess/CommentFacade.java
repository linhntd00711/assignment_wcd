/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import Entity.Comment;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author xuanvinh
 */
@Stateless
public class CommentFacade extends AbstractFacade<Comment> implements CommentFacadeLocal {

    @PersistenceContext(unitName = "AssignmentPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommentFacade() {
        super(Comment.class);
    }

    @Override
    public List<Comment> listComment(int postId) {
        Query query = em.createNativeQuery("select * from comment where post_id = ? order by create_at desc", Comment.class);
        
        query.setParameter(1, postId);
        
        List<Comment> list = (List<Comment>) query.getResultList();
        
        return list;
    }

    @Override
    public boolean addComment(String username, int postId, String content) {
        Query query = em.createNativeQuery("insert into comment(username, post_id, content, create_at) values (?, ?, ?, ?)");
        
        Date date = new Date();
        
        query.setParameter(1, username);
        query.setParameter(2, postId);
        query.setParameter(3, content);
        query.setParameter(4, date);
        
        return query.executeUpdate() != 0;
    }
    
}
